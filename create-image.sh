#!/usr/bin/env bash

docker rm -f review-service

docker rmi review-service

docker image prune

docker volume prune

docker build -t review-service .