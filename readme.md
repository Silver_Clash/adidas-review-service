## Adidas coding challenge - Review service

### Stack

*   NodeJS 8.3.0
*   NeDB - as a simple persistent data store with Mongo - like API

### how to run it

**This to run our microservice in a docker container** You need to create Docker image (see below) and run it

**To run in locally** You need nodejs installed

*   run integration (API) tests

        mocha integration-tests\index.js

*   run unit tests (jsut an example with 1 simple test)

        npm test

*   start the service locally

        node src\index.js

    OR

        npm start

*   Create docker image

        create-image.sh

*   Run docker image

        run-container.sh

    OR

        docker run --name review-service -p 3001:3001 -d review-service

### how to use it

**There is Postman collection available to simplify API testing**

*   Just check file:

        /integration-tests/Adidas - Product and Review Services.postman_collection.json

*   **Basic auth**

    Do not forget, all endpoints implying write operations require authentication (basic auth adidas:zaragoza)
*   **Self-signed certificate**

    On test/development environment used self-signed certificate, so, just in case something not working at all, try to check if requests not blocked by chrome (or other tool) due to self-signed certificate warning