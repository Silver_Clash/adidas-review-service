// just ignoring self-signed certificate errors for test purposes
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

const argv = require('optimist').argv;
const status = require('http-status');
const supertest = require('supertest');

describe('Review Service', () => {
  const api = supertest('https://localhost:' + (argv.port || 3001));
  const ts = (new Date()).getTime();
  
  const notExistedObjectUrl = '/review/never-existed-product' + ts;
  const newObjectUrl = '/review/new-product' + ts;
  
  it('Can NOT find review for not existed product', (done) => {
    api.get(notExistedObjectUrl)
      .expect(status.NOT_FOUND, done);
  });
  
  it('Can find seeded data (review for C77154 product)', (done) => {
    api.get('/review/C77154')
      .expect(status.OK)
      .expect((res) => {
        if (!res.body || res.body.product_id !== 'C77154'){
           throw new Error('Product id do not match (check seeded data)');
        }
      })
      .end(done);
  });
  
  it('Require authentication for write operations', (done) => {
    api.post('/review/never-existed-product')
      .send({"score_avg": 4, "reviews_cnt": 4})
      .expect(status.UNAUTHORIZED)
      .end(done);
  });
  
  it('Can create new object', (done) => {
    api.post(newObjectUrl)
      .auth('adidas', 'zaragoza')
      .send({"score_avg": 4, "reviews_cnt": 4})
      .expect(status.CREATED)
      .expect((res) => {
        if (!res.body || res.body.score_avg !== 4){
           throw new Error('Inserted data do not match');
        }
      })
      .end(done);
  });
  
  it('But can NOT do it twice', (done) => {
    api.post(newObjectUrl)
      .auth('adidas', 'zaragoza')
      .send({"score_avg": 4, "reviews_cnt": 4})
      .expect(status.CONFLICT)
      .end(done);
  });
  
  it('Only in case of "upsert" key presented in payload', (done) => {
    api.post(newObjectUrl)
      .auth('adidas', 'zaragoza')
      .send({"score_avg": 1, "reviews_cnt": 1, "upsert": true})
      .expect(status.OK)
      .end(done);
  });
  
  it('Can update (PUT) existed object', (done) => {
    api.put(newObjectUrl)
      .auth('adidas', 'zaragoza')
      .send({"score_avg": 1, "reviews_cnt": 1})
      .expect(status.OK)
      .expect((res) => {
        if (!res.body || res.body.score_avg !== 1){
           throw new Error('Updated data do not match');
        }
      })
      .end(done);
  });
  
  it('Can update (PATCH) existed object', (done) => {
    api.patch(newObjectUrl)
      .auth('adidas', 'zaragoza')
      .send({"score_avg": 10, "reviews_cnt": 10})
      .expect(status.OK)
      .expect((res) => {
        if (!res.body || res.body.score_avg !== 10){
           throw new Error('Updated data do not match');
        }
      })
      .end(done);
  });
  
  it('And changed data will be presented', (done) => {
    api.get(newObjectUrl)
      .expect(status.OK)
      .expect((res) => {
        if (!res.body || res.body.score_avg !== 10){
           throw new Error('Updated data do not match');
        }
      })
      .end(done);
  });
  
  it('Can NOT update (PUT) not existed object', (done) => {
    api.put(notExistedObjectUrl)
      .auth('adidas', 'zaragoza')
      .send({"score_avg": 1, "reviews_cnt": 1})
      .expect(status.NOT_FOUND)
      .end(done);
  });
  
  it('Can NOT update (PATCH) not existed object', (done) => {
    api.patch(notExistedObjectUrl)
      .auth('adidas', 'zaragoza')
      .send({"score_avg": 1, "reviews_cnt": 1})
      .expect(status.NOT_FOUND)
      .end(done);
  });
  
  it('Can delete existed object', (done) => {
    api.delete(newObjectUrl)
      .auth('adidas', 'zaragoza')
      .expect(status.OK)
      .end(done);
  });
  
  it('And deleted object will not be found', (done) => {
    api.get(newObjectUrl)
      .expect(status.NOT_FOUND, done);
  });
  
  
});