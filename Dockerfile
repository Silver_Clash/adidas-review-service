# Node v8 as the base image
FROM node:8.3.0
# New user for new container to avoid the root user
RUN useradd --user-group --create-home --shell /bin/false adidas_challenge && \
    apt-get clean
ENV HOME=/home/adidas_challenge
COPY package.json $HOME/app/

COPY src/ $HOME/app/src
COPY src/repository/reviews.database.dump $HOME/app/src/repository/reviews.database

RUN chown -R adidas_challenge:adidas_challenge $HOME/* /usr/local/
WORKDIR $HOME/app
RUN npm cache verify && \
    npm install --silent --progress=false --production
RUN chown -R adidas_challenge:adidas_challenge $HOME/*
USER adidas_challenge
EXPOSE 3001
CMD ["npm", "start"]
