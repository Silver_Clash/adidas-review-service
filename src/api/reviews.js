'use strict';
const status = require('http-status');

module.exports = (app, options) => {
  const {repo} = options;
  
  /*
   * CREATE endpoint
   * sometimes POST requests are very usefull for updates too (upsert option)
   * if in request body will be specified upsert key with not false value,
   * upsert option will be applied and conflict error never fired
   */
  app.post('/review/:pid', (req, res, next) => {
    const body = req.body;
    repo.createReview(req.params.pid, body, body.upsert).then(review => {
      res.status(body.upsert ? status.OK : status.CREATED).json(review);
    }).catch(err => {
      next(err);
    });
  });
  
  /*
   * READ endpoint
   */
  app.get('/review/:pid', (req, res, next) => {
    repo.getReviewByProductId(req.params.pid).then(review => {
      if (!review) {
        return res.sendStatus(status.NOT_FOUND);
      }
      res.status(status.OK).json(review);
    }).catch(next);
  });
  
  /*
   * UPDATE endpoints
   */
  app.patch('/review/:pid', (req, res, next) => {
    repo.updateReview(req.params.pid, req.body).then(review => {
      if (!review) {
        return res.sendStatus(status.NOT_FOUND);
      }
      res.status(status.OK).json(review);
    }).catch(next);
  });
  app.put('/review/:pid', (req, res, next) => {
    repo.updateReview(req.params.pid, req.body).then(review => {
      if (!review) {
        return res.sendStatus(status.NOT_FOUND);
      }
      res.status(status.OK).json(review);
    }).catch(next);
  });
  
  /*
   * DELETE endpoint
   */
  app.delete('/review/:pid', (req, res, next) => {
    repo.deleteReview(req.params.pid).then(cnt => {
      res.sendStatus(cnt ? status.OK : status.NOT_FOUND);
    }).catch(next);
  });
};