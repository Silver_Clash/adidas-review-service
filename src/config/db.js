const Datastore = require('nedb');

const connect = (options, mediator) => {
  mediator.once('boot.ready', () => {
    const db = new Datastore(options.dbParameters);
    db.ensureIndex({ fieldName: 'product_id', unique: true }, (err) => {
      if (err) {
        mediator.emit('db.error', err);
      }
      mediator.emit('db.ready', db);
    });
  });
};


module.exports = Object.assign({}, {connect});