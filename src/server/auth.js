'use strict';
const auth = require('basic-auth');
const status = require('http-status');
const credentials = require('./auth.config');

const authorizer = (name, pass) => {
  return credentials.name === name && credentials.pass === pass;
};
  
/*
 * Super simple auth
 */
module.exports = (req, res, next) => {
  if (req.method === 'GET') {
    return next();
  }
  var authentication = auth(req);
  if (!authentication || !authorizer(authentication.name, authentication.pass)) {
    return res.sendStatus(status.UNAUTHORIZED);
  }
  return next();
};