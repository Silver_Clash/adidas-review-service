const api = require('../api/reviews');
const basicAuth = require('./auth');
const bodyparser = require('body-parser');
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const spdy = require('spdy');

const start = (options) => {
  return new Promise((resolve, reject) => {
    if (!options.repo) {
      reject(new Error('The server must be started with a connected repository'));
    }
    if (!options.port) {
      reject(new Error('The server must be started with an available port'));
    }

    const app = express();
    app.use(morgan('dev'));
    app.use(bodyparser.json());
    app.use(helmet());
    app.use(basicAuth);
    
    api(app, options);
    
    
    app.use((err, req, res, next) => {
      reject(new Error('Something went wrong!, err:' + err));
      res.sendStatus(err.statusCode);
    });

    const server = spdy.createServer(options.ssl, app)
        .listen(options.port, () => {
          resolve(server);
        });
  });
};

module.exports = Object.assign({}, {start});