'use strict';
const status = require('http-status');

const repository = (db) => {
  /*
   * Create record method with upsert option
   */
  const createReview = (pid, data, upsert = false) => {
    return new Promise((resolve, reject) => {
      const doc = {
        product_id: pid,
        score_avg: data['score_avg'],
        reviews_cnt: data['reviews_cnt']
      };
      
      const requestHandler = (err, newDoc) => {
        if (err) {
          const error = new Error(`An error occured creating a review for product with id: ${pid}, err: ${err}`);
          // in case of uniqueViolated error sending conflict (409) error
          if (err.errorType === 'uniqueViolated') {
            error.statusCode = status.CONFLICT;
          }
          reject(error);
        }
        resolve(newDoc);
      };
      
      if (upsert) {
        db.update({product_id: pid}, doc, {upsert: true, returnUpdatedDocs: true}, (err, cnt, doc) => {
          delete doc._id;
          requestHandler(err, doc);
        });
      } else {
        db.insert(doc, requestHandler);
      }
    }).catch((err) => {
      throw err;
    });
  };
  
  /*
   * Get (find) method
   */
  const getReviewByProductId = (pid) => {
    return new Promise((resolve, reject) => {
      const sendReview = (err, review) => {
        if (err) {
          reject(new Error(`An error occured fetching a review for product with id: ${pid}, err: ${err}`));
        }
        resolve(review);
      };
      db.findOne({product_id: pid}, {_id: 0}, sendReview);
    }).catch((err) => {
      throw err;
    });
  };

  /*
   * Update method
   */
  const updateReview = (pid, data) => {
    return new Promise((resolve, reject) => {
      const doc = {
        product_id: pid,
        score_avg: data['score_avg'],
        reviews_cnt: data['reviews_cnt']
      };
      db.update({product_id: pid}, doc, {returnUpdatedDocs: true}, (err, cnt, doc) => {
        if (err) {
          reject(err);
        }
        if (doc && doc._id) {
          delete doc._id;
        }
        resolve(doc);
      });
    }).catch((err) => {
      throw err;
    });
  };
  
  /*
   * Delete method
   */
  const deleteReview = (pid) => {
    return new Promise((resolve, reject) => {
      db.remove({product_id: pid}, {}, (err, cnt) => {
        if (err) {
          reject(err);
        }
        resolve(cnt);
      });
    }).catch((err) => {
      throw err;
    });
  };
  
  return Object.create({
    createReview,
    deleteReview,
    getReviewByProductId,
    updateReview
  });
};

const connect = (connection) => {
  return new Promise((resolve, reject) => {
    if (!connection) {
      reject(new Error('connection db not supplied!'));
    }
    resolve(repository(connection));
  });
};

module.exports = Object.assign({}, {connect});